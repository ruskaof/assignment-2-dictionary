%include "lib.inc"

section .text

global find_word

find_word:
	; rsi - dict pointer
	; rdi - key pointer
	mov r12, rsi ; Save them locally so they are not lost after string_equals call
	mov r13, rdi
	.loop: 
        test r12, r12
		je .no_such_element

		mov rdi, r13
		mov rsi, r12
		add rsi, 8
		call string_equals

		test rax, rax
		jnz .found 
		mov r12, [r12]
		jmp .loop

	.found:
		mov rax, r12
		ret

	.no_such_element:
        xor rax, rax
        ret
