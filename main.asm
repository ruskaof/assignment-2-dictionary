%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%define BF_LEN 255

section .rodata

notFound: 
    db "not found in dict", 0
tooLong: 
    db "your key is too long", 0

%include 'words.inc'

section .bss
buffer: RESB BF_LEN


global _start

section .text

_start:
	; rdi - buf pointer
    ; rsi - buf size
    mov rdi, buffer
    mov rsi, BF_LEN
    call read_word
    test rax, rax
    je .too_big

    mov rdi, rax ; buff address
    mov rsi, currFirst; first element pointer
    call find_word
    test rax, rax
    je .no_such_element2

    ; rax - element address

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rax, rdi
    inc rax ; + null terminator
    mov rdi, rax

    call print_string
    call exit

    .too_big:
        mov rdi, tooLong
        mov rsi, 0
        call print_string
        call exit

    .no_such_element2:
        mov rdi, notFound
        mov rsi, 0
        call print_string
        call exit
