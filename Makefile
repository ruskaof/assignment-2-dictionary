ASM=nasm
ASMFLAGS=-felf64

main: main.o dict.o lib.o
	ld -o $@ $^

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) $<
