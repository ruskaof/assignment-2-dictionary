%define currFirst 0


; Nested if's do not work and I don't know how to do them :(
%macro colon 2
	%ifstr %1
		%ifid %2
			%2:
				dq currFirst       
				db %1, 0     
				%define currFirst %2
		%else
			%error "Illegal argument: 2 argument must be an id"
		%endif
	%else
		%error "Illegal argument: 1 argument must be a string"
	%endif
%endmacro
